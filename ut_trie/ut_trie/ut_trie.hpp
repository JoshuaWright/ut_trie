#pragma once
/** @file ut_trie.hpp
	@author Garth Santor
	@date 2017-04-30
	@version 1.1.0
	@note Visual C++ 14.1
	@brief trie<> unit test common elements headers.
	*/


#define UT_VERSION "1.1.0"
#define TEST_PHASE 69

#include <trie.hpp>
#include <gats/unit_test_phase_list.hpp>

#define BEGIN_PHASE(_num,_name) BOOST_AUTO_TEST_CASE(_name) { auto phaseNumber = _num;
#define END_PHASE() PhaseList<>::implements(phaseNumber); }


/*============================================================================

Revision History

Version 1.1.0: 2017-04-30
Updated for VC++ 14.1

Version 1.0.0: 2016-05-09
Initial version

==============================================================================
Copyright Garth Santor, 2016

The copyright to the computer program(s) herein
is the property of Garth Santor, Canada.
The program(s) may be used and/or copied only with
the written permission of Garth Santor
or in accordance with the terms and conditions
stipulated in the agreement/contract under which
the program(s) have been supplied.
============================================================================*/
