#if !defined(GUARD_trie_hpp)
#define GUARD_trie_hpp

/**	@file trie.hpp
	@author Joshua Wright
	@date 2017-05-17
	@version 1.0.0
	@note Developed for C++11/vc14
	@breif trie<T> template implementation.
	*/


#include <string>
#include <iterator>
#include <array>


template<typename T>
class trie {
public:

	// TYPES
	using mapped_type					= T;
	using mapped_type_pointer			= mapped_type*;
	using mapped_type_reference			= mapped_type&;
	using mapped_type_const_reference	= mapped_type const&;
	using size_type						= std::size_t;
	using key_type						= std::string;
	using value_type					= std::pair<key_type const, mapped_type>;
	using value_type_pointer			= value_type*;
	using value_type_reference			= value_type&;
	using value_type_const_reference	= value_type const&;
	using difference_type				= std::ptrdiff_t;

private:

	/*
	Brief: struct to represent trie node
	*/
	struct node {	

		// TYPES
		using pointer			= node*;
		using reference			= node&;
		using key_chunk_type	= char;
		using const_reference	= node const&;
		using array_type		= std::array<pointer, 256>;

		// CONTRUCTORS 
		node() { children.fill(nullptr); }

		node(char kc,key_type k) : key_chunk(kc), value(k,mapped_type()) 
		{ children.fill(nullptr); }

		node(char kc, key_type k, mapped_type&& m) : 
			key_chunk(kc), 
			value(k,std::move(m)), 
			is_terminal(true)
		{ children.fill(nullptr); }

		node(char kc, key_type k, mapped_type const& m) :
			key_chunk(kc),
			value(k, m),
			is_terminal(true)
		{ children.fill(nullptr); }

		// DESTRUCTOR
		~node() {}

		// MEMBERS	
		bool						is_terminal					= false;
		size_type					num_terminal_descendants	= 0;
		typename trie::value_type	value;
		key_chunk_type				key_chunk					= static_cast<char>(0);
		node*						parent						= nullptr;
		array_type					children;

		// FUNCTIONS
		int get_ascii() const { return static_cast<int>(key_chunk); }
		int children_size() const;
		bool has_children() const { return children_size() != 0; }

		bool operator==(node const& n) const;
		bool operator!=(node const& n) const { return !operator==(n); }

	}; // end struct node

public:

	/*
	Brief: Iterator class for trie 
	*/
	class iterator : public std::iterator<std::bidirectional_iterator_tag,value_type> {
	private:
		
		/*
		Brief: enum to help with traversal 
		*/
		enum _state {
			down = 0,
			up,
			left,
			going_down,
			going_up
		};
		
		// MEMBERS
		mutable node*				currentNode = nullptr;
		mutable trie<T> const*		container	= nullptr;
		mutable _state				currentState;
		
	public:

		// CONTRUCTORS
		iterator() {}

		iterator(iterator const& it) 
			: container(it.container),
			currentNode(it.currentNode), 
			currentState(it.currentState) {}

		iterator(trie* c, node* n = nullptr) 
			: container(c), currentNode(n) {}

		iterator(trie const* c, node* n = nullptr) 
			: container(c), currentNode(n) {}

		// DESTRUCTOR
		~iterator() {}
		
		// MEMBER ACCESS
		value_type*				operator->() { return &currentNode->value; }
		value_type&				operator*() { return currentNode->value; }
		value_type const*		operator->() const { return &currentNode->value;}
		value_type const&		operator*() const { return currentNode->value; }

		// ASSIGNMENT 
		iterator&		operator=(iterator const& rhs);

		// INCREMENT / DECREMENT 
		iterator&	operator++();		
		iterator	operator++(int);
		iterator&	operator--();
		iterator	operator--(int);

		iterator const&	operator++()	const;
		iterator const	operator++(int) const;
		iterator const&	operator--()	const;
		iterator const operator--(int)	const;


		// RELATIONAL OPERATORS 
		bool operator == (iterator const& rhs) const;
		bool operator != (iterator const& rhs)const { return !operator==(rhs); }

		friend trie;
		
	private:

		// HELPERS 
		void _forward_execute					(node*&, node*&) const;
		void _forward_first_terminal_descendant	(node*&, node*&) const;
		void _forward_up						(node*&, node*&) const;
		void _forward_down						(node*&, node*&) const;
		void _forward_first_child				(node*&, node*&) const;
		void _forward_first_sibling				(node*&, node*&) const;

		void _backward_execute						(node*&, node*&) const;
		void _backward_first_terminal_descendant	(node *&, node*&) const;
		void _backward_up_left						(node*&, node*&) const;
		void _backward_first_child					(node*&, node*&) const;
		void _backward_first_sibling				(node*&, node*&) const;

	}; // end class iterator


	// TYPES
	using const_iterator			= iterator const;
	using reverse_iterator			= std::reverse_iterator<iterator>;
	using const_reverse_iterator	= std::reverse_iterator<const_iterator>;
	using insert_return_type		= std::pair<iterator,bool>;


private:

	// MEMBERS
	mutable node* root;

public:

	// CONTRUCTORS 
	trie() : root(new node) {}
	trie(trie const&);
	trie(trie&& other);
	trie(std::initializer_list<value_type> const&);

	template< class InputIterator >
	trie(InputIterator first, InputIterator last);

	// DESTRUCTOR
	~trie();

	// ELEMENT ACCESS
	mapped_type_reference			operator[](key_type const&);
	mapped_type_reference			operator[](char*);
	mapped_type_reference			front() { return begin()->second; }
	mapped_type_reference			back() { return (--end())->second; }

	// ASSIGNMENT 
	trie&		operator=(trie const&);
	trie&		operator=(trie &&);
	trie&		operator=(std::initializer_list<value_type> const&);

	// MODIFIERS
	void				clear();
	insert_return_type	insert(value_type&&);
	insert_return_type	insert(value_type const&);
	void				insert(std::initializer_list<value_type> const&);

	template< class InputIt >
	void insert(InputIt first, InputIt last);

	void				swap(trie<T>&);
	iterator			erase(iterator const&);
	size_type			erase(key_type const&);
	iterator			erase(iterator const&,iterator const&);

	// ITERATORS
	iterator				begin();
	iterator				end() { return iterator(this); }
	const_iterator			begin() const { return cbegin(); }
	const_iterator			end() const { return cend(); }
	const_iterator			cbegin() const;
	const_iterator			cend() const { return const_iterator(this); }

	reverse_iterator		rbegin() { return reverse_iterator(end()); }
	reverse_iterator		rend() { return reverse_iterator(begin()); }
	const_reverse_iterator	rbegin() const { return crbegin(); }
	const_reverse_iterator	rend() const { return crend(); }
	const_reverse_iterator	crbegin() const { return const_reverse_iterator(cend()); }
	const_reverse_iterator	crend() const { return const_reverse_iterator(cbegin()); }

	// CAPACITY
	size_type	size()		const { return root->num_terminal_descendants; }
	bool		empty()		const { return size() == 0; }
	size_type	max_size()	const { return std::numeric_limits<size_type>::max(); }

	// LOOKUP
	size_type	count(key_type const& key)	const;
	iterator	find(key_type const& key)	const;

	// RELATIONAL 
	bool operator == (trie const&) const;
	bool operator > (trie const&)  const;
	bool operator != (trie const& rhs) const { return !operator==(rhs); }
	bool operator < (trie const& rhs)  const { return rhs.operator>(*this); }
	bool operator >= (trie const& rhs) const { return !operator>(rhs); }
	bool operator <= (trie const& rhs) const { return !operator<(rhs); }

private:

	// HELPERS
	void	find_node_loop_recursion(node*& n, key_type key, int& position, node*& result) const;
	void	find_loop_recursion(node* n, key_type key, int& position, iterator& result) const;
	void	insert_from_root(node*,int&,value_type &&,insert_return_type&);
	void	insert_from_under_root(node*,int&,value_type &&, insert_return_type&);
	void	insert_from_root(node*, int&, value_type const&, insert_return_type&);
	void	insert_from_under_root(node*, int&, value_type const&, insert_return_type&);
	void	clearn_loop_recursion(node* n);
	node*	find_node(key_type const& key) const;
	int		get_ascii(char c) const { return static_cast<int>(c); }
	void	erase_loop_recursion(iterator&,iterator&);

}; // end class



// TIRE IMPLEMENTATIONS 

template<typename T>
trie<T>::trie(trie && other) {
	root = std::move(other.root);
	other.root = new node();
}

template<typename T>
trie<T>::trie(trie const & other) : root(new node) {
	for (auto& it : other) {
		insert(value_type(it.first,it.second));
	}
}

template<typename T>
template<class InputIterator>
trie<T>::trie(InputIterator first, InputIterator last) : root(new node) {
	insert(first, last);
}

template<typename T>
trie<T>::trie(std::initializer_list<value_type> const & init) : root(new node) {
	for (auto e : init)
		insert(e);
}

template<typename T>
trie<T>::~trie() {
	clear();
	delete root;
}

template<typename T>
trie<T>& trie<T>::operator=(trie && rhs) {
	clear();
	delete root;
	root = std::move(rhs.root);
	rhs.root = new node();
	return *this;
}

template<typename T>
trie<T>& trie<T>::operator=(trie const & rhs) {
	clear();
	for (auto& it : rhs) {
		insert(value_type(it.first, it.second));
	}
	return *this;
}

template<typename T>
bool trie<T>::operator==(trie const & rhs) const {
	if (size() != rhs.size())
		return false;

	auto this_it = cbegin();
	auto rhs_it = rhs.cbegin();
	while(this_it != end()) {
		if ( *rhs_it++ != *this_it++ )
			return false;
	}
	return true;
}

template<typename T>
bool trie<T>::operator>(trie const& rhs) const {
	// TODO:
	return false;
}

template<typename T>
trie<T>& trie<T>::operator=(std::initializer_list<value_type> const & init) {
	clear();
	for (auto e : init)
		insert(e);
	return *this;
}

template<typename T>
typename trie<T>::mapped_type& trie<T>::operator[](key_type const& key) {
	node* n = find_node(key);
	if (n == nullptr) {
		insert_return_type result = insert(value_type(key,mapped_type()));
		return result.first->second;
	}
	if (!n->is_terminal) {
		n->is_terminal = true;

		if (key.length() == 1) 
			++(n->parent->num_terminal_descendants);
		else {
			++(n->parent->num_terminal_descendants);
			++root->num_terminal_descendants;
		}
	}
	return n->value.second;
}

template<typename T>
typename trie<T>::mapped_type& trie<T>::operator[](char* key) {
	node* n = find_node(key);
	if (n == nullptr) {
		insert_return_type result = insert(value_type(key, mapped_type()));
		return result.first->second;
	}
	n->is_terminal = true;
	return n->value.second;
}

template<typename T>
void trie<T>::swap(trie<T>& rhs) {
	node* temp = rhs.root;
	rhs.root = root;
	root = temp;
}

template<typename T>
typename trie<T>::size_type trie<T>::count(key_type const& key) const
{
	if (key == "")
		return root->num_terminal_descendants;

	node* result = find_node(key);
	if (result == nullptr)
		return 0;
	if (result->is_terminal) {
		return result->num_terminal_descendants + 1;
	}
	return result->num_terminal_descendants;
}

template<typename T>
typename trie<T>::node* trie<T>::find_node(key_type const& key) const {
	if (key == "")
		return root;

	int level = 1;
	node* result = nullptr;
	node* child = root->children[get_ascii(key.at(0))];

	if (child != nullptr) {
		find_node_loop_recursion(child, key, level, result);
		return result;
	}
	return result;
} 

template<typename T>
void trie<T>::find_node_loop_recursion(node*& n,key_type key, int& level,node*& result) const
{
	if (key.size() == level) {
		result = n;
		return;
	}

	node* child = n->children[get_ascii(key.at(level))];

	if (child != nullptr) {
		find_node_loop_recursion(child, key, ++level, result);
		return;
	}
	// Key not in trie

} // end find_node_loop_recursion

template<typename T>
typename trie<T>::iterator trie<T>::find(key_type const& key) const
{
	int position = 0;
	iterator result = iterator(this, nullptr);
	for (node*& child : root->children) {
		find_loop_recursion(root, key, position, result);
		break;
	}
	return result;
}

template<typename T>
void trie<T>::find_loop_recursion(node* n,key_type key, int& level,iterator& result) const  {
	if (key.size() == level) {
		result = iterator(this, n);
		return;
	}

	node* child = n->children[get_ascii(key.at(level))];

	if (child != nullptr) {
		find_loop_recursion(child, key, ++level, result);
		return;
	}
	// Key not in trie

} //  end find_loop_recursion

template<typename T>
typename trie<T>::insert_return_type trie<T>::insert(value_type&& toBeInserted) {
	insert_return_type result;
	int level = 0;

	// test for insert into root
	if (toBeInserted.first.empty()) {
		result.second = false;
		result.first = iterator(this, root);
		if (root->is_terminal == false) {
			root->is_terminal = true;
			root->value.second = std::move(toBeInserted.second);
			++root->num_terminal_descendants;
		}
		return result;
	}

	node* child = root->children[get_ascii(toBeInserted.first.at(0))];

	if (child == nullptr) {
		// insert from root
		insert_from_root(root, level, std::move(toBeInserted), result);
		return result;
	}

	// insert from below root
	insert_from_under_root(child, level, std::move(toBeInserted), result);
	return result;
}

template<typename T>
typename trie<T>::insert_return_type trie<T>::insert(value_type const& toBeInserted) {
	insert_return_type result;
	int level = 0;

	// test for insert into root
	if (toBeInserted.first.empty()) {	
		result.second = false;
		result.first = iterator(this, root);
		if (root->is_terminal == false) {
			root->is_terminal = true;
			root->value.second = toBeInserted.second;
			++root->num_terminal_descendants;
		}
		return result;	
	}

	node* child = root->children[get_ascii(toBeInserted.first.at(0))];

	if (child == nullptr) {
		// insert from root
		insert_from_root(root, level, toBeInserted, result);
		return result;
	}

	// insert from below root
	insert_from_under_root(child, level, toBeInserted, result);
	return result;
}

template<typename T>
void trie<T>::insert(std::initializer_list<value_type> const& init) {
	for (auto e : init)
		insert(e);
}

template<typename T>
template<class InputIt>
void trie<T>::insert(InputIt first, InputIt last) {
	while (first != last)
		insert(* first ++ );
}

template<typename T>
void trie<T>::insert_from_root(node* n, int& level,value_type&& toBeInserted, insert_return_type& result) {
	// incrementing counts
	++n->num_terminal_descendants;

	// checking if end of key has been reached 
	if (toBeInserted.first == toBeInserted.first.substr(0, level +1)) {
		node* lastNode = new node(toBeInserted.first.back(), toBeInserted.first,std::move(toBeInserted.second));
		lastNode->parent = n;
		n->children[lastNode->get_ascii()] = lastNode;
		result.second = true;
		result.first = iterator(this, lastNode);
		return;
	}

	// not the end create a new node a repeat
	node* onTheWayNode = new node(toBeInserted.first.at(level), toBeInserted.first.substr(0, level + 1));
	onTheWayNode->parent = n;
	n->children[onTheWayNode->get_ascii()] = onTheWayNode;
	insert_from_root(onTheWayNode, ++level, std::move(toBeInserted),result);
} 

template<typename T>
void trie<T>::insert_from_under_root(node* n, int& level, value_type&& toBeInserted, insert_return_type& result) {
	++level;

	// checking in key already exist 
	if (toBeInserted.first.size() == level) {
		result.second = false;
		result.first = iterator(this, n);
		if (n->is_terminal == false) {
			n->is_terminal = true;
			n->value.second = std::move(toBeInserted.second);
			if (*n->parent == *root) {
				++root->num_terminal_descendants;
			}
			else {
				++n->parent->num_terminal_descendants;
				++root->num_terminal_descendants;
			}
		}
		return;
	}

	// checking if current nodes children has key chunk 
	node* child = n->children[get_ascii(toBeInserted.first.at(level))];
	if (child != nullptr) {
		if (child->key_chunk == toBeInserted.first.at(level)) {
			insert_from_under_root(child, level, std::move(toBeInserted), result);
			return;
		}
	}

	++n->num_terminal_descendants;

	// if key chunk dose exist make new branch 
	if (toBeInserted.first.size() > toBeInserted.first.substr(0, level + 1).size()) {
		node* notLastNode = new node(toBeInserted.first.at(level), toBeInserted.first.substr(0, level + 1));
		notLastNode->parent = n;
		n->children[(notLastNode->get_ascii())] = notLastNode;
		insert_from_under_root(notLastNode, level, std::move(toBeInserted), result);
		return;
	}

	++root->num_terminal_descendants;

	// if key chunk dose not exist and is last 
	node* lastNode = new node(toBeInserted.first.back(), toBeInserted.first, std::move(toBeInserted.second));
	lastNode->parent = n;
	n->children[lastNode->get_ascii()] = lastNode;
	result.second = true;
	result.first = iterator(this, lastNode);
	return;
} // end insert_existing_loop_recursion

template<typename T>
void trie<T>::insert_from_root(node* n, int& level, value_type const& toBeInserted, insert_return_type& result) {
	// incrementing counts
	++n->num_terminal_descendants;

	// checking if end of key has been reached 
	if (toBeInserted.first == toBeInserted.first.substr(0, level + 1)) {
		node* lastNode = new node(toBeInserted.first.back(), toBeInserted.first, toBeInserted.second);
		lastNode->parent = n;
		n->children[lastNode->get_ascii()] = lastNode;
		result.second = true;
		result.first = iterator(this, lastNode);
		return;
	}

	// not the end create a new node a repeat
	node* onTheWayNode = new node(toBeInserted.first.at(level), toBeInserted.first.substr(0, level + 1));
	onTheWayNode->parent = n;
	n->children[onTheWayNode->get_ascii()] = onTheWayNode;
	insert_from_root(onTheWayNode, ++level, toBeInserted, result);
}

template<typename T>
void trie<T>::insert_from_under_root(node* n, int& level, value_type const& toBeInserted, insert_return_type& result) {
	++level;

	// checking in key already exist 
	if (toBeInserted.first.size() == level) {
		result.second = false;
		result.first = iterator(this, n);
		if (n->is_terminal == false) {
			n->is_terminal = true;
			n->value.second = toBeInserted.second;
			if (*n->parent == *root) {
				++root->num_terminal_descendants;
			}
			else {
				++n->parent->num_terminal_descendants;
				++root->num_terminal_descendants;
			}
		}
		return;
	}

	// checking if current nodes children has key chunk 
	node* child = n->children[get_ascii(toBeInserted.first.at(level))];
	if (child != nullptr) {
		if (child->key_chunk == toBeInserted.first.at(level)) {
			insert_from_under_root(child, level, toBeInserted, result);
			return;
		}
	}

	++n->num_terminal_descendants;

	// if key chunk dose exist make new branch 
	if (toBeInserted.first.size() > toBeInserted.first.substr(0, level + 1).size()) {
		node* notLastNode = new node(toBeInserted.first.at(level), toBeInserted.first.substr(0, level + 1));
		notLastNode->parent = n;
		n->children[(notLastNode->get_ascii())] = notLastNode;
		insert_from_under_root(notLastNode, level, toBeInserted, result);
		return;
	}

	++root->num_terminal_descendants;

	// if key chunk dose not exist and is last 
	node* lastNode = new node(toBeInserted.first.back(), toBeInserted.first, toBeInserted.second);
	lastNode->parent = n;
	n->children[lastNode->get_ascii()] = lastNode;
	result.second = true;
	result.first = iterator(this, lastNode);
	return;
} // end insert_existing_loop_recursion

template<typename T>
void trie<T>::clear() {
	clearn_loop_recursion(root);
} // end clear 

template<typename T>
void trie<T>::clearn_loop_recursion(node* n) {
	for (int i = 0; i < n->children.size(); ++i) {
		if (n->children[i] != nullptr) {
			clearn_loop_recursion(n->children[i]);
			if (n->children[i]->is_terminal) {
				--root->num_terminal_descendants;
			}
			delete n->children[i];
			n->children[i] = nullptr;
		}
	}
} // end clearn_loop_recursion

template<typename T>
typename trie<T>::iterator trie<T>::erase(iterator const& it) {
	if (it.currentNode == nullptr)
		return it;

	if (*it.currentNode == *root) {
		return end();
	}

	iterator result(it);
	++result;

	if(*it.currentNode->parent == *root) {
		--root->num_terminal_descendants;
	}
	else {
		--(it.currentNode->parent->num_terminal_descendants);
		--root->num_terminal_descendants;
	}	

	if (it.currentNode->num_terminal_descendants > 0) {
		it.currentNode->is_terminal = false;
		return result;
	}

	iterator parent(this, it.currentNode->parent);

	it.currentNode->parent->children[it.currentNode->get_ascii()] = nullptr;
	delete (it.currentNode);
	it.currentNode = nullptr;


	erase_loop_recursion(parent, result);
	return result;
}

template<typename T>
void trie<T>::erase_loop_recursion(iterator& it, iterator& result) {
	if (it.currentNode->has_children() == false)
		it.currentNode->num_terminal_descendants = 0;
	if (it.currentNode->has_children() || it.currentNode->is_terminal) 
		return;

	if (it.currentNode->parent != nullptr) {
		iterator parent(this, it.currentNode->parent);
		it.currentNode->parent->children[it.currentNode->get_ascii()] = nullptr;
		delete (it.currentNode);
		it.currentNode = nullptr;
		result = parent;
		erase_loop_recursion(parent,result);
	}
	// at root 
}

template<typename T>
typename trie<T>::iterator trie<T>::erase(iterator const& beg_it, iterator const& end_it) {
	iterator current(beg_it);
	while (current != end_it) {
		current = erase(current);
	}
	return current;
}

template<typename T>
typename trie<T>::size_type trie<T>::erase(key_type const & key) {
	node* toBeErased = find_node(key);
	if (toBeErased == nullptr)
		return 0;

	erase(iterator(this, toBeErased));
	return 1;
}

template<typename T>
typename trie<T>::iterator trie<T>::begin() {
	if (root->has_children() == false)
		return end();

	iterator it(this);
	it._forward_first_terminal_descendant(root,it.currentNode);
	return it;
}

template<typename T>
typename const trie<T>::iterator trie<T>::cbegin() const {
	if (root->has_children() == false)
		return end();

	const_iterator it(this);
	it._forward_first_terminal_descendant(root, it.currentNode);
	return it;
}



// TIRE::NODE IMPLEMENTATIONS 

template<typename T>
int trie<T>::node::children_size() const {
	int not_null_count = 0;
	for (typename array_type::size_type i = 0; i < children.size(); ++i) {
		if (children[i] != nullptr)
			++not_null_count;
	}
	return not_null_count;
}

template<typename T>
bool trie<T>::node::operator==(node const & rhs) const {
	if (is_terminal == rhs.is_terminal &&
		num_terminal_descendants == rhs.num_terminal_descendants &&
		value == rhs.value &&
		key_chunk == rhs.key_chunk &&
		parent == rhs.parent &&
		children_size() == rhs.children_size())
		return true;
	return false;
}



// TIRE::ITERATOR IMPLEMENTATIONS 

template<typename T>
typename trie<T>::iterator& trie<T>::iterator::operator=(iterator const& rhs) {
	container = rhs.container;
	currentNode = rhs.currentNode;
	currentState = rhs.currentState;
	return *this;
}

template<typename T>
void trie<T>::iterator::_forward_first_child(node *& n, node *& result) const {
	for (unsigned int i = 0; i < n->children.size(); ++i) {
		if (n->children[i] != nullptr) {
			result = n->children[i];
			return;
		}
	}
}

template<typename T>
void trie<T>::iterator::_backward_first_child(node *& n, node *& result)  const{
	for (std::size_t i = n->children.size() - 1; i >= 0; --i) {
		if (n->children[i] != nullptr) {
			result = n->children[i];
			return;
		}
	}
}

template<typename T>
void trie<T>::iterator::_backward_first_sibling(node *& n, node*& result) const {
	for (int i = n->get_ascii() - 1; i >= 0; --i) {
		if (n->parent->children[i] != nullptr) {
			result = n->parent->children[i];
			return;
		}
	}
}

template<typename T>
void trie<T>::iterator::_forward_first_sibling(node *& n, node*& result) const {
	for (unsigned int i = n->get_ascii() + 1; i < n->children.size(); ++i) {
		if (n->parent->children[i] != nullptr) {
			result = n->parent->children[i];
			return;
		}
	}
}

template<typename T>
void trie<T>::iterator::_forward_first_terminal_descendant(node *& n, node *& result) const{
	if (n->is_terminal) {
		currentState = _state::down;
		result = n;
		return;
	}	
	node* childNode = nullptr;

	_forward_first_child(n, childNode);

	if (childNode == nullptr) {
		return;
	}
	result = childNode;
	_forward_first_terminal_descendant(result, result);
}

template<typename T>
void trie<T>::iterator::_backward_first_terminal_descendant(node *& n, node *& result) const{
	if (n->has_children() == false) {
		result = n;
		return;
	}
	node* childNode = nullptr;

	_backward_first_child(n, childNode);

	if (childNode == nullptr) {
		return;
	}
	result = childNode;
	_backward_first_terminal_descendant(result, result);
}

template<typename T>
void trie<T>::iterator::_forward_execute(node *& n, node *& result)  const{
	if (n->has_children() == true) 
		_forward_down(n, result);
	else 
		_forward_up(n, result);
}

template<typename T>
void trie<T>::iterator::_forward_down(node *& n, node *& result) const{

	// STOPPING CONDITION
	if (n->is_terminal && (currentState == _state::going_down)) {
		currentState = _state::down;
		result = n;
		return;
	}

	node* childNode = nullptr;
	_forward_first_child(n, childNode);

	if (childNode != nullptr) {
		// STOPPING CONDITION
		if (childNode->is_terminal == false) {
			_forward_down(childNode, result);
			return;
		}
		currentState = _state::going_up;
		result = childNode;
		return;
	}

	//node* parentNode = nullptr;
	//parentNode = n->parent;

	//// STOPPING CONDITION
	//if (parentNode == nullptr) {
	//	currentState = _state::going_down;
	//	return;
	//}

	//_forward_up(parentNode, result);

	//currentPhase = phase::up;
	//_forward_strategy::up(parentNode, result);

	/*node* parentNode = nullptr;
	parentNode = n->parent;

	if (parentNode == nullptr)
		return;

	node* siblingNode = nullptr;
	_forward_strategy::first_sibling(n, siblingNode);

	if (siblingNode == nullptr) {
		n = parentNode;

		_forward_strategy::up(parentNode, result);
		return;
	}
	else if (siblingNode->is_terminal) {
		result = siblingNode;
		return;
	}
	else {
		_forward_strategy::first_child(siblingNode, childNode);
		return;
	}*/
}

template<typename T>
void trie<T>::iterator::_forward_up(node *& n, node *& result) const{

	// STOPPING CONDITION
	if (n->is_terminal &&  (currentState == _state::up)) {
		result = n;
		return;
	}

	node* parentNode = nullptr;
	parentNode = n->parent;

	// STOPPING CONDITION
	if (parentNode == nullptr) {
		currentState = _state::going_down;
		return;
	}

	node* siblingNode = nullptr;
	_forward_first_sibling(n, siblingNode);

	if (siblingNode == nullptr) {
		_forward_up(parentNode, result);
		return;
	}
	
	currentState = _state::going_down;
	_forward_down(siblingNode, result);
}

template<typename T>
void trie<T>::iterator::_backward_execute(node *& n, node *& result) const {
	_backward_up_left(n, result);
}

template<typename T>
void trie<T>::iterator::_backward_up_left(node *& n, node *& result) const {
	// STOPPING CONDITION
	if (n->is_terminal && (currentState == _state::up)) {
		currentState = _state::left;
		result = n;
		return;
	}

	node* parentNode = nullptr;
	parentNode = n->parent;

	// STOPPING CONDITION
	if (parentNode == nullptr) {
		return;
	}

	node* siblingNode = nullptr;
	_backward_first_sibling(n, siblingNode);

	// STOPPING CONDITION
	if (siblingNode == nullptr) {
		currentState = _state::up;
		_backward_up_left(parentNode, result);
		return;
	}

	currentState = _state::left;
	_backward_first_terminal_descendant(siblingNode,result);
}

template<typename T>
bool trie<T>::iterator::operator ==(iterator const& rhs) const{
	if (container == nullptr && rhs.container == nullptr)
		return true;
	if (container == nullptr)
		return false;
	if (container == rhs.container) {
		if (currentNode == rhs.currentNode)
			return true;
	}
	return false;
}

template<typename T>
typename trie<T>::iterator& trie<T>::iterator::operator++() {
	if (currentNode != nullptr) {
		node* result = nullptr;
		_forward_execute(currentNode, result);
		currentNode = result;
	}
	// throw out_of_range
	return *this;
}

template<typename T>
typename const trie<T>::iterator& trie<T>::iterator::operator++() const {
	if (currentNode != nullptr) {
		node* result = nullptr;
		_forward_execute(currentNode, result);
		currentNode = result;
	}
	// throw out_of_range
	return *this;
}

template<typename T>
typename trie<T>::iterator& trie<T>::iterator::operator--() {
	if (currentNode == nullptr) {
		currentState = _state::left;
		_backward_first_terminal_descendant(container->root, currentNode);
	}
	else {
		node* result = nullptr;
		_backward_execute(currentNode, result);
		currentNode = result;
	}
	return *this;
}

template<typename T>
typename const trie<T>::iterator& trie<T>::iterator::operator--() const{
	if (currentNode == nullptr) {
		currentState = _state::left;
		_backward_first_terminal_descendant(container->root, currentNode);
	}
	else {
		node* result = nullptr;
		_backward_execute(currentNode, result);
		currentNode = result;
	}
	return *this;
}

template<typename T>
typename trie<T>::iterator trie<T>::iterator::operator++(int) {
	iterator copy(*this);
	operator++();
	return copy;
}

template<typename T>
typename const trie<T>::iterator trie<T>::iterator::operator++(int) const {
	const_iterator copy(*this);
	operator++();
	return copy;
}

template<typename T>
typename trie<T>::iterator trie<T>::iterator::operator--(int) {
	iterator copy(*this);
	operator--();
	return copy;
}

template<typename T>
typename const trie<T>::iterator trie<T>::iterator::operator--(int) const {	
	const_iterator copy(*this);
	operator--();
	return copy;
}

#endif // GUARD_trie_hpp