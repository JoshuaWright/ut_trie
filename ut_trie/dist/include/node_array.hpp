/*
Author:	Joshua wright joshuawright1197@gmail.com
Date:	2017-05-21
File:	sorted_array.hpp
*/

#pragma once

#include <iterator>
#include <exception>
#include <cstdlib>

template<typename T,std::size_t N,typename Compare = std::less<T>>
class node_array {
public:

	// TYPES
	using size_type = std::size_t;
	using value_type = T;
	using value_compare = Compare;
	using pointer = value_type*;
	using const_pointer = const value_type*;
	using reference = value_type&;
	using const_reference = const value_type&;
	using iterator = pointer;
	using const_iterator = const pointer;
	using reverse_iterator = std::reverse_iterator<iterator>;
	using const_reverse_iterator = std::reverse_iterator<const_iterator>;

private:

	// MEMBERS
	pointer			begPtr[N];
	pointer			endUsedPtr;
	pointer			endPtr;

public:

	// CONSTRUCTORS
	sorted_array() : begPtr(nullptr), endPtr(nullptr) {}
	sorted_array(value_type const&);
	//sorted_array(std::initializer_list<T> const&);
	sorted_array(sorted_array const&);

	// DESTRUCTOR
	~sorted_array() { clear(); }

	// ELEMENT ACCESS
	reference		operator [] (size_type const& idx) { return begPtr[idx]; }
	const_reference operator [] (size_type const& idx) const { return begPtr[idx]; }
	void			insert(value_type const&);
	reference		at(size_type const);
	const_reference at(size_type const) const;
	reference		back()			{ return *(endPtr - 1); }
	const_reference	back() const	{ return *(endPtr - 1); }
	reference		front()			{ return *begPtr; }
	const_reference	front() const	{ return *begPtr; }
	pointer			data()			{ return begPtr; }
	const_pointer	data() const	{ return begPtr; } 

	// CAPACITY
	size_type		size() const { return N; }
	bool			empty() const { return N == 0;}

	// ITERATORS
	iterator				begin() { return iterator(begPtr); }
	iterator				end() { return iterator(endPtr); }
	const_iterator			begin()	const { return const_iterator(begPtr); }
	const_iterator			end() const { return const_iterator(endPtr); }
	const_iterator			cbegin() const { return const_iterator(begPtr); }
	const_iterator			cend() const { return const_iterator(endPtr); }
	reverse_iterator		rbegin() { return reverse_iterator(endPtr); }
	reverse_iterator		rend() { return reverse_iterator(begPtr); }
	const_reverse_iterator	rbegin() const { return const_reverse_iterator(endPtr); }
	const_reverse_iterator	rend() const { return const_reverse_iterator(begPtr); }
	const_reverse_iterator	crbegin() const { return const_reverse_iterator(endPtr); }
	const_reverse_iterator	crend() const { return const_reverse_iterator(begPtr); }

	// MODIFIERS
	void			clear();
	void			fill(value_type const&);
	void			swap(sorted_arra&);
	sorted_array&	operator=(sorted_array const&);

private:
	// HELPERS  
	void sort(size_type);

}; // end class Vector

template<typename T, std::size_t N, typename Compare = std::less<T>>
sorted_array<T,Compare>::sorted_array(value_type const& value) {
	endPtr = begPtr + N;
	for (size_type i = 0; i < N; ++i) {
		begPtr[i] = value;
	}
}

//template<typename T, std::size_t N, typename Compare = std::less<T>>
//sorted_array<T,Compare>::sorted_array(std::initializer_list<T> const& init) {
//	capPtr = endPtr = begPtr = allocator.allocate(init.size());
//	capPtr += init.size();
//	for (auto& e : init)
//		insert(e);
//}

template<typename T, std::size_t N, typename Compare = std::less<T>>
sorted_array<T, Compare>::sorted_array(sorted_array const& toBeCopyed) {
	begPtr = allocator.allocate(toBeCopyed.capacity());
	capPtr = begPtr + toBeCopyed.capacity();

	for (unsigned i = 0; i < toBeCopyed.size(); ++i)
		allocator.construct(begPtr + i, toBeCopyed[i]);

	endPtr = begPtr + toBeCopyed.size();
}

template<typename T, std::size_t N, typename Compare = std::less<T>>
sorted_array<T, Compare, Allocator>& sorted_array<T, Compare, Allocator>::operator=(sorted_array const& toBeCopyed)
{
	clear();

	if ((size() == toBeCopyed.size()) && (capacity() == toBeCopyed.capacity())) {
		for (unsigned i = 0; i < toBeCopyed.size(); ++i)
			begPtr[i] == toBeCopyed[i];
		return *this;
	}

	begPtr = allocator.allocate(toBeCopyed.capacity());
	capPtr = begPtr + toBeCopyed.capacity();

	for (unsigned i = 0; i < toBeCopyed.size(); ++i)
		allocator.construct(begPtr + i, toBeCopyed[i]);

	endPtr = begPtr + toBeCopyed.size();

	return *this;
}

template<typename T, std::size_t N, typename Compare = std::less<T>>
void sorted_array<T, Compare, Allocator>::insert(value_type const& value) {
	if (endPtr == capPtr) {
		int newCap = 0;

		if (size() != 0)
			newCap = (capacity() * 2);
		else
			newCap = 1;

		reAllocate(newCap);
	}
	begPtr[size()] = value;
	++endPtr;
	sort(size() - 1);
}

template<typename T, std::size_t N, typename Compare = std::less<T>>
typename sorted_array<T, Compare, Allocator>::value_type sorted_array<T, Compare, Allocator>::pop_back() {
	value_type result = begPtr[size() - 1];
	allocator.destroy(begPtr + (size() - 1));
	--endPtr;
	return result;
}

template<typename T, std::size_t N, typename Compare = std::less<T>>
void sorted_array<T, Compare, Allocator>::clear() {
	for (pointer p = begPtr; p != endPtr; ++p)
		allocator.destroy(p);
	allocator.deallocate(begPtr, size());
}

template<typename T, std::size_t N, typename Compare = std::less<T>>
typename sorted_array<T, Compare, Allocator>::reference sorted_array<T, Compare, Allocator>::at(size_type const idx) {
	if (idx >= size())
		throw std::out_of_range("index out of range");
	return begPtr[idx];
}

template<typename T, std::size_t N, typename Compare = std::less<T>>
typename sorted_array<T, Compare, Allocator>::const_reference sorted_array<T, Compare, Allocator>::at(size_type const idx) const {
	if (idx >= size())
		throw std::out_of_range("index out of range");
	return begPtr[idx];
}

template<typename T, std::size_t N, typename Compare = std::less<T>>
void sorted_array<T, Compare, Allocator>::sort(size_type index) {
	if (index > 0) {
		if (Compare()(begPtr[index], begPtr[index - 1])) {
			std::swap(begPtr[index], begPtr[index - 1]);
			return sort(--index);
		}
	}
}

